#!/usr/bin/env nextflow

config = Channel.fromPath('msmfs-3504.in')
datafiles = Channel.fromPath('askapsdp-3504.ms')

process image {
  echo true
  input: 
    val x from config
    val df from datafiles  
  output:
    file 'image.cont.taylor.0.restored' into restored_image
  script:
    """
    ln -s $df ./askapsdp-3504.ms;
    mpirun -np 4 imager -c $x
    """
}
process test {
  echo true
  input:
    file ri from restored_image
  script:
  """
    imgstat $ri
  """
}

